<?php


/**
 * Класс NotFoundController отвечает за обработку ситуации, когда запрошенный ресурс не найден.
 */
class NotFoundController
{

    /**
     * Действие для обработки запроса на несуществующую страницу.
     */
    public function actionIndex()
    {
        ResponseService::error(404);
    }

}