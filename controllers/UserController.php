<?php


/**
 * Контроллер, отвечающий за управление пользователями.
 */
class UserController
{

    /**
     * Создает новый экземпляр класса UserController.
     * Проверяет аутентификацию пользователя при создании экземпляра.
     */
    public function __construct()
    {
        AuthenticationService::authenticate();
    }


    /**
     * Действие для получения всех пользователей.
     *
     * @throws Exception Если возникла ошибка при получении пользователей.
     */
    public function actionIndex()
    {

        try {
            $users = User::getAllUsers();
            ResponseService::success($users);
        } catch (Exception $exception) {
            ResponseService::error(404);
        }

    }


    /**
     * Действие для получения пользователя по идентификатору.
     *
     * @param int $id Идентификатор пользователя.
     *
     * @throws Exception Если возникла ошибка при получении пользователя.
     */

    public function actionShow($id)
    {
        try {
            $user = User::getUserById($id);
            if (isset($user)) {
                ResponseService::success($user);
            } else {
                ResponseService::error(404);
            }

        } catch (Exception $exception) {
            ResponseService::error(404);
        }
    }


}