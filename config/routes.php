<?php


return array(


    'users/([0-9]+)'=>'user/show/$1',
    'users'=>'user/index',

    'notfound' => 'notFound/index',

);