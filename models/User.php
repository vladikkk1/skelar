<?php

/**
 * Класс, представляющий пользователя.
 *
 * Реализует интерфейс JsonSerializable для сериализации объекта в JSON.
 */
class User implements JsonSerializable
{

    /**
     * Идентификатор пользователя.
     *
     * @var int
     */
    private int $id;

    /**
     * Имя пользователя.
     *
     * @var string
     */
    private string $name;


    /**
     * Пол пользователя.
     *
     * @var string
     */
    private string $gender;

    /**
     * Возраст пользователя.
     *
     * @var int
     */
    private int $age;


    /**
     * Адрес пользователя.
     *
     * @var string
     */
    private string $address;


    /**
     * Конструктор класса User.
     *
     * @param int $id Идентификатор пользователя.
     * @param string $name Имя пользователя.
     * @param string $gender Пол пользователя.
     * @param int $age Возраст пользователя.
     * @param string $address Адрес пользователя.
     */
    public function __construct(int $id, string $name, string $gender, int $age, string $address)
    {
        $this->id = $id;
        $this->name = $name;
        $this->gender = $gender;
        $this->age = $age;
        $this->address = $address;
    }

    /**
     * Возвращает идентификатор пользователя.
     *
     * @return int Идентификатор пользователя.
     */

    public function getId(): int
    {
        return $this->id;
    }


    /**
     * Возвращает имя пользователя.
     *
     * @return string Имя пользователя.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Возвращает пол пользователя.
     *
     * @return string Пол пользователя.
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * Возвращает возраст пользователя.
     *
     * @return int Возраст пользователя.
     */
    public function getAge(): int
    {
        return $this->age;
    }


    /**
     * Возвращает адрес пользователя.
     *
     * @return string Адрес пользователя.
     */
    public function getAddress(): string
    {
        return $this->address;
    }


    /**
     * Получает все элементы коллекции пользователей.
     *
     * @return User[] Массив пользователей.
     */
    public static function getAllUsers(): array
    {
        return [
            new User(1, 'User 1', 'Male', 30, '1 Street'),
            new User(2, 'User 2', 'Male', 55, '2 Street'),
            new User(3, 'User 3', 'Female', 25, '3 Street'),
            new User(4, 'User 4', 'Female', 26, '4 Street')
        ];
    }

    /**
     * Получает пользователя по его идентификатору.
     *
     * @param int $id Идентификатор пользователя.
     *
     * @return User|null Пользователь или null, если пользователь не найден.
     */
    public static function getUserById($id): ?User
    {
        $items = self::getAllUsers();
        foreach ($items as $item) {
            if ($item->getId() == $id) {
                return $item;
            }
        }
        return null;
    }


    /**
     * Возвращает массив данных пользователя для сериализации в JSON.
     *
     * @return array Массив данных пользователя.
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'gender' => $this->gender,
            'age' => $this->age,
            'address' => $this->address,
        ];
    }
}
