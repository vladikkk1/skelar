<?php


/**
 * Class Auth
 * Модель для аутентификации
 */
class Auth
{
    /**
     * @var string Логин пользователя
     */
    private string $username;

    /**
     * @var string Пароль пользователя
     */
    private string $password;

    /**
     * Auth constructor.
     * @param string $username Логин пользователя
     * @param string $password Пароль пользователя
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }


    /**
     * Получить логин пользователя
     * @return string Логин пользователя
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Получить пароль пользователя
     * @return string Пароль пользователя
     */
    public function getPassword(): string
    {
        return $this->password;
    }

}