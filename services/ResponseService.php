<?php

class ResponseService
{


    private static array $statusHeaders = [
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed'
    ];

    /**
     * Возвращает успешный ответ в формате JSON.
     *
     * @param mixed $data Данные, которые нужно вернуть в ответе.
     * @param int $statusCode (optional) Код состояния HTTP ответа (по умолчанию 200 OK).
     *
     * @return void
     */

    public static function success(mixed $data, int $statusCode = 200): void
    {
        header('Content-Type: application/json');
        http_response_code($statusCode);
        echo json_encode(['data' => $data]);
        exit();
    }


    /**
     * Возвращает ошибочный ответ в формате JSON.
     *
     * @param string $message Сообщение об ошибке.
     * @param int $statusCode Код состояния HTTP ответа.
     *
     * @return void
     */
    public static function error(int $statusCode): void
    {
        header('Content-Type: application/json');
        http_response_code($statusCode);
        echo json_encode(['error' => self::$statusHeaders[$statusCode]]);
        exit();
    }
}