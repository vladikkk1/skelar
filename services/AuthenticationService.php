<?php


class AuthenticationService
{


    /**
     * Аутентификация пользователя с использованием базовой аутентификации.
     * Если авторизация не предоставлена или учетные данные неверны, будет возвращен статус 401 Unauthorized.
     * В случае успешной аутентификации, функция просто завершит выполнение без возвращаемого значения.
     *
     * @return void
     */

    public static function authenticate(): void
    {

        self::checkRequestMethod();

        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
            // Авторизация не предоставлена
            header('WWW-Authenticate: Basic realm="Restricted Area"');
            ResponseService::error(401);
        }

        $authUser = new Auth('admin', 'password');

        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];

        // Проверка учетных данных пользователя
        if ($username !== $authUser->getUsername() || $password !== $authUser->getPassword()) {
            ResponseService::error(401);
        }

    }

    /**
     * Проверка типа запроса
     * @return void
     */
    public static function checkRequestMethod(): void
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            ResponseService::error(405);
            exit();
        }
    }

}